package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("post.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setSubject(request.getParameter("subject"));
			message.setText(request.getParameter("text"));
			message.setCategory(request.getParameter("category"));
			message.setRegisteredperson_id(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			String subject = request.getParameter("subject");
			String text = request.getParameter("text");
			String category = request.getParameter("category");
			request.setAttribute("subject", subject);
			request.setAttribute("text", text);
			request.setAttribute("category", category);
			request.getRequestDispatcher("post.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(subject) == true || StringUtils.isBlank(subject) == true) {
			messages.add("件名を入力してください");
		}

		if (subject.length() > 30) {
			messages.add("件名は30文字以下で入力してください");
		}

		if (StringUtils.isEmpty(text) == true || StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}

		if (text.length() > 1000) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (StringUtils.isEmpty(category) == true || StringUtils.isBlank(category) == true) {
			messages.add("カテゴリを入力してください");
		}

		if (category.length() > 10) {
			messages.add("カテゴリは10文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}