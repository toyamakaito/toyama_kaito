package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();

			user.setName(request.getParameter("name"));
			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

			new UserService().register(user);

			response.sendRedirect("usermanagement");
		} else {
			String name = request.getParameter("name");
			String login_id = request.getParameter("login_id");
			String password = request.getParameter("password");
			String passwordConfirm = request.getParameter("passwordConfirm");
			int branch_id = Integer.parseInt(request.getParameter("branch_id"));
			int position_id = Integer.parseInt(request.getParameter("position_id"));
			session.setAttribute("errorMessages", messages);
			request.setAttribute("name", name);
			request.setAttribute("login_id", login_id);
			request.setAttribute("password", password);
			request.setAttribute("passwordConfirm", passwordConfirm);
			request.setAttribute("branch_id", branch_id);
			request.setAttribute("position_id", position_id);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));
		LoginService loginService = new LoginService();
        User login_id2 = loginService.login_id2(login_id);


		if (StringUtils.isEmpty(name) == true) {
			messages.add("ユーザー名を入力してください");
		}else if (name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");

		} else if (login_id.length() < 6) {
			messages.add("ログインIDは6文字以上で入力してください");
		} else if (login_id.length() > 20) {
			messages.add("ログインIDは20文字以内で入力してください");
		} else if (!(login_id.matches("[*A-Za-z0-9]+"))) {
			messages.add("ログインIDは半角英数字で入力して");
		}
		if(login_id2!=null) {

			messages.add("ログインIDが重複しています");
		}


		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}else if(!(password.matches("[*A-Za-z0-9!:@¥.]+"))) {
			messages.add("パスワードは半角英数字で入力してください");

		}else if (password.length() < 6) {
			messages.add("パスワードは6文字以上で入力してください");
		} else if (password.length() > 20) {
			messages.add("パスワードは20文字以内で入力してください");
		}

		if (password.equals(passwordConfirm) == false) {
			messages.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if(branch_id==1){
			if(!(position_id==1||position_id==2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}
		}
		if(branch_id==2||branch_id==3||branch_id==4) {
			if(!(position_id==3||position_id==4)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
