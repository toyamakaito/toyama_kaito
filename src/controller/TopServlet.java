package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserCommentes;
import beans.UserMessage;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



        String time1 = request.getParameter("time1");
        String time2 = request.getParameter("time2");
        String category=request.getParameter("category");
        request.setAttribute("time1", time1);
        request.setAttribute("time2", time2);
        request.setAttribute("category", category);

        List<UserMessage> messages = new MessageService().getMessage(time1,time2,category);
        List<UserCommentes> commentes = new MessageService().getCommentes();

        request.setAttribute("messages", messages);
        request.setAttribute("commentes", commentes);




        request.getRequestDispatcher("/top.jsp").forward(request, response);

    }
}
