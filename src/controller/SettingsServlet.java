package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.LoginService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> messages2 = new ArrayList<String>();

		if(request.getParameter("id")==null) {
			messages2.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages2);
			response.sendRedirect("usermanagement");
			return;
		}



		int userId = Integer.parseInt(request.getParameter("id"));
		User edituser = new UserService().getUser(userId);

		if (edituser == null) {

			messages2.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages2);
			response.sendRedirect("usermanagement");
		} else {
			request.setAttribute("edituser", edituser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User edituser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(edituser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("edituser", edituser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			response.sendRedirect("usermanagement");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("edituser", edituser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User edituser = new User();
		edituser.setId(Integer.parseInt(request.getParameter("id")));
		edituser.setLogin_id(request.getParameter("login_id"));
		edituser.setName(request.getParameter("name"));
		edituser.setPassword(request.getParameter("password"));
		edituser.setBranch(request.getParameter("branch"));
		edituser.setPosition(request.getParameter("position"));
		edituser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		edituser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
		return edituser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));
		LoginService loginService = new LoginService();
		User login_id2 = loginService.login_id2(login_id);
		int userId = Integer.parseInt(request.getParameter("id"));
		User edituser = new UserService().getUser(userId);

		if (StringUtils.isEmpty(name) == true) {
			messages.add("ユーザー名を入力してください");
		} else if (name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		} else if (login_id.length() < 6) {
			messages.add("ログインIDは6文字以上で入力してください");
		} else if (login_id.length() > 20) {
			messages.add("ログインIDは20文字以内で入力してください");
		} else if (!(login_id.matches("[*A-Za-z0-9]+"))) {
			messages.add("ログインIDは半角英数字で入力して");
		}
		if (!(login_id.equals(edituser.getLogin_id()))) {

			if (login_id2 != null) {

				messages.add("ログインIDが重複しています");
			}
		}
		if (StringUtils.isEmpty(password) == false) {
			if (!(password.matches("[*A-Za-z0-9!:@¥.]+"))) {
				messages.add("パスワードは半角英数字で入力してください");
			} else if (password.length() < 6) {
				messages.add("パスワードは6文字以上で入力してください");
			} else if (password.length() > 20) {
				messages.add("パスワードは20文字以内で入力してください");
			}
		}

		if (password.equals(passwordConfirm) == false) {
			messages.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if (branch_id == 1) {
			if (!(position_id == 1 || position_id == 2)) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}
		if (branch_id == 2 || branch_id == 3 || branch_id == 4) {
			if (!(position_id == 3 || position_id == 4)) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}