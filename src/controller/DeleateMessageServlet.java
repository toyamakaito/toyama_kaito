package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DeleateMessage;
import service.MessageService;

@WebServlet(urlPatterns = { "/deleatemessage" })
public class DeleateMessageServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {




        DeleateMessage deleatemessage = new DeleateMessage();

        deleatemessage.setId(Integer.parseInt(request.getParameter("id")));
        new MessageService().deleate(deleatemessage);
        response.sendRedirect("./");

}
}