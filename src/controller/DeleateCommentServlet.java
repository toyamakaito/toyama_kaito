package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DeleateComment;
import service.MessageService;

@WebServlet(urlPatterns = { "/deleatecomment" })
public class DeleateCommentServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {




		DeleateComment deleatecomment = new DeleateComment();

		deleatecomment.setId(Integer.parseInt(request.getParameter("id")));
        new MessageService().deleate2(deleatecomment);
        response.sendRedirect("./");

}
}