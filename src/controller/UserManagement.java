package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/usermanagement" })
public class UserManagement extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {



        //ログインユーザー情報のidを元にDBからユーザー情報取得
        List<User> users = new UserService().getUser();
        request.setAttribute("users", users);


        request.getRequestDispatcher("usermanagement.jsp").forward(request, response);
    }
}