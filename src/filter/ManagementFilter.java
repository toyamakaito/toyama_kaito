package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import beans.User;

@WebFilter({"/usermanagement","/signup","/settings"})
public class ManagementFilter implements Filter{
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException,ServletException{


        // セッションが存在しない場合NULLを返す
    	User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");

        if(user.getPosition_id()==1) {
            // セッションがNULLでなければ、通常どおりの遷移
            chain.doFilter(request, response);
        }else{
            // セッションがNullならば、ログイン画面へ飛ばす

        	List<String> messages = new ArrayList<String>();
        	messages.add("権限がありません");
        	request.setAttribute("errorMessages", messages);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/.");
            dispatcher.forward(request,response);
        }

    }

    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}