package beans;

import java.io.Serializable;
import java.util.Date;

public class Commentes implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int registeredperson_id;
    private int post_id;
    private String text;
    private String registeredperson;
    private Date createdDate;
    private Date updated_date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRegisteredperson_id() {
		return registeredperson_id;
	}
	public void setRegisteredperson_id(int registeredperson_id) {
		this.registeredperson_id = registeredperson_id;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRegisteredperson() {
		return registeredperson;
	}
	public void setRegisteredperson(String registeredperson) {
		this.registeredperson = registeredperson;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}







}