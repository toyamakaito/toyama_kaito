package beans;

import java.io.Serializable;
import java.util.Date;

public class UserCommentes implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int post_id;
    private int registeredperson_id;
    private String registeredperson;
    private String subject;
    private String text;
    private Date created_date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	public int getRegisteredperson_id() {
		return registeredperson_id;
	}
	public void setRegisteredperson_id(int registeredperson_id) {
		this.registeredperson_id = registeredperson_id;
	}
	public String getRegisteredperson() {
		return registeredperson;
	}
	public void setRegisteredperson(String registeredperson) {
		this.registeredperson = registeredperson;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}


}