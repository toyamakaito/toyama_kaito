package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserCommentes;
import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection ,String start,String end,String categorysearch ) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("postes.id as id, ");
            sql.append("postes.subject as subject, ");
            sql.append("postes.text as text, ");
            sql.append("postes.category as category, ");
            sql.append("postes.registeredperson_id as registeredperson_id, ");
            sql.append("users.name as name, ");
            sql.append("postes.created_date as created_date ");
            sql.append("FROM postes ");
            sql.append("INNER JOIN users ");
            sql.append("ON postes.registeredperson_id = users.id ");
            sql.append(" WHERE postes.created_date BETWEEN ? AND ?");
            sql.append(" AND postes.category LIKE ?" );
            sql.append("ORDER BY created_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1,start);
            ps.setString(2,end);
            ps.setString(3,"%"+categorysearch+"%");


            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                int registeredperson_id = rs.getInt("registeredperson_id");
                int id = rs.getInt("id");


                Timestamp created_date = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setName(name);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setId(id);
                message.setRegisteredperson_id(registeredperson_id);
                message.setCreated_date(created_date);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public List<UserCommentes> getCommentes(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("commentes.id as id, ");
            sql.append("commentes.text as text, ");
            sql.append("commentes.registeredperson as registeredperson, ");
            sql.append("commentes.registeredperson_id as registeredperson_id, ");
            sql.append("commentes.post_id as post_id, ");
            sql.append("commentes.created_date as created_date ");
            sql.append("FROM commentes ");
            sql.append("INNER JOIN postes ");
            sql.append("ON commentes.post_id = postes.id ");
            sql.append("ORDER BY created_date ASC ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserCommentes> ret = toCommentes(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserCommentes> toCommentes(ResultSet rs)
            throws SQLException {

        List<UserCommentes> ret = new ArrayList<UserCommentes>();
        try {
            while (rs.next()) {
                String registeredperson = rs.getString("registeredperson");
                String text = rs.getString("text");
                int registeredperson_id = rs.getInt("registeredperson_id");
                int id = rs.getInt("id");
                int post_id = rs.getInt("post_id");


                Timestamp created_date = rs.getTimestamp("created_date");

                UserCommentes commentes = new UserCommentes();
                commentes.setRegisteredperson(registeredperson);
                commentes.setText(text);
                commentes.setPost_id(post_id);
                commentes.setId(id);
                commentes.setRegisteredperson_id(registeredperson_id);
                commentes.setCreated_date(created_date);

                ret.add(commentes);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}