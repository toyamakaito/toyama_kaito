package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Commentes;
import beans.DeleateComment;
import beans.DeleateMessage;
import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO postes ( ");
            sql.append("subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", registeredperson_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // subject
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", ?"); // registeredperson_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, message.getSubject());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getRegisteredperson_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void insert2(Connection connection, Commentes commentes) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO commentes ( ");
            sql.append("text");
            sql.append(", registeredperson");
            sql.append(", registeredperson_id");
            sql.append(", post_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // text
            sql.append(", ?"); // registeredperson
            sql.append(", ?");//Registeredperson_id
            sql.append(", ?");//Post_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, commentes.getText());
            ps.setString(2, commentes.getRegisteredperson());
            ps.setInt(3, commentes.getRegisteredperson_id());
            ps.setInt(4, commentes.getPost_id());



            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void deleate(Connection connection,DeleateMessage deleatemessage) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM postes WHERE id = ?; ");


            ps = connection.prepareStatement(sql.toString());


            ps.setInt(1, deleatemessage.getId());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void deleate2(Connection connection,DeleateComment deleatecomment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM commentes WHERE id = ?; ");


            ps = connection.prepareStatement(sql.toString());


            ps.setInt(1, deleatecomment.getId());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}