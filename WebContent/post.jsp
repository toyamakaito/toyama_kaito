<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style4.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
	<div class="head">
		新規投稿 <a href="./">戻る</a>
</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	<form action="newMessage" method="post">
		<br />
		<div class="post">
			<div class="subject">
				<input class="ef" placeholder="" type="text" name="subject"
					id="subject" value="${subject}" /><label for="subject">件名</label><span
					class="focus_line"><i></i></span> <br />
			</div>
			<div class="text">

				<textarea class="ef" placeholder="" name="text" cols="100" rows="20"
					class="text">${text}</textarea>
				<label for="text">本文</label><span class="focus_line"><i></i></span>
			</div>
			<div class="category">
				<input class="ef" placeholder="" type="text" name="category"
					id="category" value="${category}" /><label for="category">カテゴリー</label>
				<span class="focus_line"><i></i></span><br />
			</div>
			<input class="btn-flat-vertical-border"type="submit" value="投稿">

		</div>
	</form>
</body>
</html>