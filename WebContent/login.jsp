<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="login" method="post">
			<div class="cp_iptxt">
				<input class="ef" type="text" placeholder="" name="login_id"
					id="login_id" value="${login_id}" /><label for="login_id">ログインID</label>
				<span class="focus_line"><i></i></span>
			</div>
			<div class="cp_iptxt2">
				<input class="ef2" type="password" placeholder="" name="password"
					 id="password" /> <label for="password">パスワード</label><span
					class="focus_line"><i></i></span>
			</div>
			<div class="botan">
			<input class="btn-flat-vertical-border"type="submit" value="ログイン" />
			</div>
		</form>


	</div>
</body>
</html>