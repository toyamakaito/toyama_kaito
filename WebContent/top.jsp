<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>簡易Twitter</title>
<link href="./css/style2.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="head">
		<a href="newMessage" class="btn-square-raised">新規投稿</a> <a
			href="usermanagement" class="btn-square-raised">ユーザー管理画面</a> <a
			href="logout" class="btn-square-raised">ログアウト</a> <a href="index.jsp"
			class="btn-square-raised">HOME</a><br />
	</div>
	<div class="main-contents">
		<div class="siborikomi">
			<form action="./" method="get">

				<input type="date" name="time1" min="2019-01-01" max="2019-12-31"
					value="${time1}"><label for="name">～</label> <input
					type="date" name="time2" min="2019-01-01" max="2019-12-31"
					value="${time2}"> <input type="text" name="category"
					value="${category}"> <input
					class="btn-flat-vertical-border" type="submit" value="しぼりこみ">

			</form>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


		<div class="messages">
			<div class="post">
				<font size=20>投稿一覧</font>
			</div>
			<c:forEach items="${messages}" var="message">
				<div class="post1">
					<div class="account-name">
						<label for="name">氏名：</label> <span class="name"><c:out
								value="${message.name}" /></span>
					</div>
					<div class="subject">
						<label for="subject">件名：</label>
						<c:out value="${message.subject}" />
					</div>

					<div class="text">
						<label for="text">本文：</label>
						<c:forEach var="s" items="${fn:split(message.text, '
')}">
							<div>${s}</div>
						</c:forEach>
					</div>

					<div class="category">
						<label for="category">カテゴリー：</label>
						<c:out value="${message.category}" />
					</div>
					<div class="date">
						<label for="date">更新日時：</label>
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</div>
				<c:if test="${ message.registeredperson_id==loginUser.id }">
					<form action="deleatemessage" method="post">
						<div class="deleate">
							<input type="hidden" name="id" value="${message.id}" /> <input
								class="btn-flat-vertical-border" type="submit" value="削除"
								onclick="return confirm('削除しますか？')">

						</div>
					</form>
				</c:if>
				<div class="commentes">
					<div class="comment">
						<font size=10>コメント</font>
					</div>
					<c:forEach items="${commentes}" var="commente">
						<c:if test="${message.id==commente.post_id}">
							<div class="message2">
								<div class="account-name">
									<span class="registeredperson"><c:out
											value="${commente.registeredperson}" /></span>
								</div>
								<div class="text2">
									<c:forEach var="s" items="${fn:split(commente.text, '
')}">
										<div>${s}</div>
									</c:forEach>
								</div>
								<div class="date">
									<fmt:formatDate value="${commente.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</div>
							<div class=deleate>
								<c:if test="${commente.registeredperson_id==loginUser.id}">
									<form action="deleatecomment" method="post">
										<input type="hidden" name="id" value="${commente.id}" /> <input
											class="btn-flat-vertical-border" type="submit" value="削除"
											onclick="return confirm('削除しますか？')">
									</form>
								</c:if>
							</div>
						</c:if>
					</c:forEach>
				</div>
				<form action="commentes" method="post">
					<div class="cp_iptxt">


						<textarea name="text" cols="100" rows="5" class="ef"
							placeholder=""></textarea>
						<input type="hidden" name="id" value="${message.id}" /> <label
							for="text">コメントを入力</label><span class="focus_line"><i></i></span></div><input
							class="btn-flat-vertical-border" type="submit" value="投稿">

				</form>
			</c:forEach>
		</div>


		<div class="copyright">Copyright(c)YourName</div>
	</div>
</body>
</html>