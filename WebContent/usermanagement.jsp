<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
        <link href="css/style3.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    <div class="head">

    	<label for="name">ユーザー情報</label>

    	<a href="signup">ユーザー新規登録画面</a>
    	<a href = "./">戻る</a>

    	<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message2">
						<li><c:out value="${message2}" />
					</c:forEach>
				</ul>
			</div>

			<c:remove var="errorMessages" scope="session" />
		</c:if>
		</div>
        <c:forEach items="${users}" var="user">

        	<div class="userslist">
        		<span class="userslist">Id：<c:out value="${user.id}" /></span>
       			<span class="userslist">名前：<c:out value="${user.name}" /></span><br />
       			<span class="userslist">ログインId：<c:out value="${user.login_id}" /></span><br />
       			<span class="userslist">支店：<c:out value="${user.branch}" /></span><br />
       			<span class="userslist">役職：<c:out value="${user.position}" /></span><br />
       			<form action="settings" method="get"><br />
        			<input type="hidden" name="id" value="${user.id}" />

				 	<input class="btn-flat-vertical-border"type="submit" value="編集">

			 	</form>
			 	<c:if test="${!(user.id==loginUser.id) }">
				<c:if test="${ user.is_stop==1 }">
			 	<form action="is_stop" method="get"><br />
			 		<input type="hidden" name="id" value="${user.id}" />
				 	<input class="btn-flat-vertical-border"type="submit" value="停止" onclick="return confirm('停止しますか？')">
			 	</form>
				</c:if>
				<c:if test="${ user.is_stop==0}">
		 		<form action="unis_stop" method="get"><br />
		 			<input type="hidden" name="id" value="${user.id}" />
				 	<input class="btn-flat-vertical-border"type="submit" value="復活" onclick="return confirm('復活しますか？')">
 				 </form>
 				 </c:if>
 				 </c:if>
        	</div>

        </c:forEach>

    </body>
</html>