<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>設定</title>
<link href="css/style5.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${edituser.id}" id="id" type="hidden" />
			<label for="name">名前</label> <input name="name"
				value="${edituser.name}" id="name" />（名前はあなたの公開プロフィールに表示されます）<br />

			<label for="login_id">ログインid</label> <input name="login_id"
				value="${edituser.login_id}" /><br /> <label for="password">パスワード</label>
			<input name="password" type="password" id="password" /> <br /> <label
				for="passwordConfirm">パスワード（確認）</label> <input
				name="passwordConfirm" type="password" id="passwordConfirm" /> <br />
			<c:if test="${edituser.id!=loginUser.id }">
				<label for="branch_id">支店コード</label>
				<select name="branch_id">
					<option value="1"
						<c:if test="${edituser.branch_id==1 }">selected</c:if>>本社</option>
					<option value="2"
						<c:if test="${edituser.branch_id==2 }">selected</c:if>>支店A</option>
					<option value="3"
						<c:if test="${edituser.branch_id==3 }">selected</c:if>>支店B</option>
					<option value="4"
						<c:if test="${edituser.branch_id==4 }">selected</c:if>>支店C</option>
				</select>
				<label for="position_id">役職・部署コード</label>
				<select name="position_id">
					<option value="1"
						<c:if test="${edituser.position_id==1 }">selected</c:if>>総務人事担当者</option>
					<option value="2"
						<c:if test="${edituser.position_id==2 }">selected</c:if>>情報管理担当者</option>
					<option value="3"
						<c:if test="${edituser.position_id==3 }">selected</c:if>>支店長</option>
					<option value="4"
						<c:if test="${edituser.position_id==4 }">selected</c:if>>社員</option>
				</select></c:if>
			<input type="hidden" name="branch" value="${edituser.branch}" /> <input
				type="hidden" name="position" value="${edituser.position}" />
				<input type="hidden" name="branch_id" value="${edituser.branch_id}" />
				<input type="hidden" name="position_id" value="${edituser.position_id}" />
				<input class="btn-flat-vertical-border"
				type="submit" value="登録" /> <br /> <a href="usermanagement">戻る</a>
		</form>

	</div>
</body>
</html>